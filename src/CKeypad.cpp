#include "CKeypad.h"

CKeypad::CKeypad(char *userKeymap, byte *row, byte *col, byte numRows, byte numCols) : Keypad(userKeymap, row, col, numRows, numCols) {}

CKeypad::~CKeypad() {
  delete this;
}

/**
* This method reads as many characters as were passed
* with the parameter countChars.
*/
const String CKeypad::read(const int countChars) {
  String result = "";
  for(int i = 0; i < countChars; i++) {
    char key = waitForKey();
    result += key;
  }
  return result;
}

const char CKeypad::readMenu() {
  String input = read(1);
  return input[0];
}

const String CKeypad::readPin() {
  String input = read(1);
  return input;
}