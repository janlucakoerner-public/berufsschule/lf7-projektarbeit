#include "CLcdI2h"

#define SECOND 1000

CLcdI2c::CLcdI2c(const byte address, const byte col, const byte row) : LiquidCrystal_I2C(address, col, row) {
  init();
  backlight();
  setCursor(0, 0);
}

CLcdI2c::~CLcdI2c() {
  delete this;
}

void CLcdI2c::showMenu() {
  clear();
  setCursor(0, 0);
  print("1: Add; 2: Login");
  setCursor(0, 1);
  print("3: Del; #: Exit");
}

void CLcdI2c::showLogin(const String pin) {
  clear();
  setCursor(0, 0);
  print("Enter Login Pin:");
  setCursor(0, 1);
  print("Input: ");
  for (int i = 0; i < pin.length(); i++) {
  	print("*");
  }
}

void CLcdI2c::showLoginResponse(const bool response) {
  clear();
  setCursor(0, 0);
  if (response) {
  	print("Login succeceded");
  } else {
  	print("Login failed");
  }
  delay(5 * SECOND);
}

void CLcdI2c::showAdd(const String pin) {
  clear();
  setCursor(0, 0);
  print("Add - Enter Pin:");
  setCursor(0, 1);
  print("Input: ");
  for (int i = 0; i < pin.length(); i++) {
  	print("*");
  }
}

void CLcdI2c::showAddResponse(const bool response) {
  clear();
  setCursor(0, 0);
  if (response) {
  	print("Login added");
  } else {
  	print("Add failed");
  }
  delay(5 * SECOND);
}

void CLcdI2c::showDelete(const String pin) {
  clear();
  setCursor(0, 0);
  print("Delete - Enter Pin:");
  setCursor(0, 1);
  print("Input: ");
  for (int i = 0; i < pin.length(); i++) {
  	print("*");
  }
}
                    
void CLcdI2c::showDeleteResponse(const bool response) {
  clear();
  setCursor(0, 0);
  if (response) {
  	print("Login deleted");
  } else {
  	print("Deletion failed");
  }
  delay(5 * SECOND);
}