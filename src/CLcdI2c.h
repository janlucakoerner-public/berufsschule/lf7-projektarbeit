#ifndef CLcdI2c_h
#define CLcdI2c_h

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

class CLcdI2c : public LiquidCrystal_I2C {
  public:
  	CLcdI2c(const byte address, const byte col, const byte row);
  	~CLcdI2c();
  	void showMenu();
  	void showLogin(const String pin = "");
  	void showLoginResponse(const bool response);
  	void showAdd(const String pin = "");
  	void showAddResponse(const bool response);
  	void showDelete(const String pin = "");
  	void showDeleteResponse(const bool response);
};

#endif