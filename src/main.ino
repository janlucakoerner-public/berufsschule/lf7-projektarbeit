#include "Safe.h"

CSafe *safe = nullptr;

void setup() {
  safe = new CSafe();
}

void loop() {
  switch (safe->menu()) {
    case '1': safe->addPin(); break;
    case '2': safe->login(); break;
    case '3': safe->deletePin(); break;
  	case '#': safe->exit(); break;
    default: break;
  }
}

