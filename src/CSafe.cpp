#include "CSafe.h"

#define PIN_COUNT 10
#define PIN_LEN 4

const byte LEN_COLUMN = 4;
const byte LEN_ROW = 4;
char characters[LEN_COLUMN][LEN_ROW] = {
  {'D','#','0','*'},
  {'C','9','8','7'},
  {'B','6','5','4'},
  {'A','3','2','1'}
};
byte columnPins[LEN_COLUMN] = {A0, A1, A2, A3};
byte rowPins[LEN_ROW] = {2, 3, 4, 5};

CSafe::CSafe() {
  keypad = new CKeypad(
  	makeKeymap(characters),
    rowPins,
    columnPins,
    LEN_ROW,
    LEN_COLUMN
  );
  lcd = new CLcdI2c(0x27, 16, 2);
  for (int i = 0; i < PIN_COUNT; i++) {
  	pins[i] = "*";
  }
}

CSafe::~CSafe() {
  delete this;
}

char CSafe::menu() {
  lcd->showMenu();
  return keypad->readMenu();
}

void CSafe::addPin() {
  String pin = "";
  for (int i = 0; i < PIN_LEN; i++) {
  	lcd->showAdd(pin);
    pin += keypad->readPin();
  }
  bool added = false;
  for (int i = 0; i < PIN_COUNT; i++) {
    if (pins[i] == "*") {
      pins[i] = pin;
      added = true;
      break;
    }
  }
  lcd->showAddResponse(added);
}

void CSafe::login() {
  String pin = "";
  for (int i = 0; i < PIN_LEN; i++) {
  	lcd->showLogin(pin);
    pin += keypad->readPin();
  }
  bool success = false;
  for (int i = 0; i < PIN_COUNT; i++) {
    if (pins[i] == pin) {
      success = true;
      break;
    }
  }
  lcd->showLoginResponse(success);
}

void CSafe::deletePin() {
  String pin = "";
  for (int i = 0; i < PIN_LEN; i++) {
  	lcd->showAdd(pin);
    pin += keypad->readPin();
  }
  bool deleted = false;
  for (int i = 0; i < PIN_COUNT; i++) {
    if (pins[i] == pin) {
      pins[i] = "*";
      deleted = true;
      break;
    }
  }
  lcd->showDeleteResponse(deleted);
}

void CSafe::exit() {
  lcd->clear();
  lcd->noBacklight();
  while (true) {}
}