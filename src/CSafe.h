#ifndef CSafe_h
#define CSafe_h

#include "CLcdI2c.h"
#include "CKeypad.h"

class CSafe {
  private:
  	CLcdI2c *lcd = nullptr;
  	CKeypad *keypad = nullptr;
  	String pins[PIN_COUNT];
  public:
  	CSafe();
  	~CSafe();
  	char menu();
  	void addPin();
  	void deletePin();
  	void login();
  	void exit();
};

#endif