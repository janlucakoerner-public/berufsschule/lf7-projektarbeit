#ifndef CKeypad_h
#define CKeypad_h

#include <Keypad.h>

class CKeypad : public Keypad {
  private:
  	const String read(const int countChars);
  public:
    CKeypad(char *userKeymap, byte *row, byte *col, byte numRows, byte numCols);	
  	~CKeypad();
  	const char readMenu();
  	const String readPin();
};

#endif