# Informationen zum Git-Repository
In diesem Git-Repository sind alle Entwicklungsobjekte des digitalen Authentifizierungssystems abgespeichert.
Des Weiteren sind unter den folgenden Links der [Projektbericht](https://gitlab.com/janlucakoerner-public/berufsschule/lf7-projektarbeit/-/blob/main/20230405_Projektdokumentation_LF7.pdf), die [Schaltskizze](https://gitlab.com/janlucakoerner-public/berufsschule/lf7-projektarbeit/-/blob/main/20230405_Schaltskizze.pdf) sowie das [Tinkercad Projekt](https://www.tinkercad.com/things/fM6tfleppXJ) sichtbar.

# Projektdokumentation
## Projektauftrag
Der Gegenstand des Projektes ist die Entwicklung eines Authentifizierungssystems für
die Sicherstellung des Zugriffsschutzes zu Tresorräumen. Die Entwicklung eines solchen
Systems wurde durch die German Credit AG, einem deutschen Kreditinstitut, am Markt
ausgeschrieben. Die Secure Safe Solutions GmbH hat als einer der Wettbewerber die
Ausschreibung gewonnen und ist nun beauftragt worden ein Prototypen zu entwerfen,
welcher die Funktionalität des späteren Gesamtsystems abbildet.

## Vorgehensweise
Um einen möglichst reibungslosen Projektprozess gewährleisten zu können, wurden am
Projektbeginn zunächst die einzelnen Phasen des Projektes definiert und in einen zeitlichen
Ablauf gebracht. Die Phasen und der Ablauf werden in dem Projektstrukturplan
näher aufgezeigt und folgend dokumentiert aufgeführt. Der Implementationsschritt in diesem
Bericht bezieht sich dabei größtenteils auf den Entstehungsprozess des Prototypens.

Die Secure Safe Solutions GmbH hat für das erfolgreiche Entwerfen eines Prototypens
und die Implementierung eines späteren Endproduktes zuerst die Anforderungen des
Auftraggebers erhoben und dokumentiert. Die Anforderungserhebung und 
Anforderungsdokumentierung fand im agilen Stil in der Form von User Stories statt.
Diese haben sich bei dem folgenden Projekt besonders geeignet, da sie grundsätzlich 
eine kurzfristige Anforderungserhebung ermöglichen und einen kundenorientierten 
Blick auf das System gewähren. Ein Auszug der erhobenen Anforderungen ist dem Anhang 
dieser Dokumentation beigefügt.

Im weiteren Verlauf sind verschiedene Lösungsansätze durch den Auftragnehmer erarbeitet
und diese anhand qualitativer Merkmale abgewägt worden. Ein näherer Ausblick
auf die verschiedenen Umsetzungsmöglichkeiten und den späteren Implementierungsfavorit,
welcher als Prototyp erstellt worden ist, gewährt das Kapitel Lösung und Alternativen.
Nachdem sich das Projektteam auf einen der Lösungsansätze geeinigt hat, wurde mit der
Detailplanung und der Konzeptionierung des Prototypens begonnen. Dieser
Schritt wurde durch ein UML-Klassendiagramm, welches den Komponentenaufbau des
Prototypens widerspiegelt, näher dokumentiert.

Im Anschluss an die Detailplanung ist der Prototyp in Form eines Arduino UNO 
hardwaretechnisch im Simulationsprogramm Tinkercad mit weiteren Komponenten verdrahtet und
softwareseitig implementiert worden. Bei der softwareseitigen Implementierung hat sich
das Entwicklerteam für den objektorientierten Ansatz entschiedenen, da dieser im Gegensatz
zu dem prozeduralen Ansatz eine hohe Kohäsion und geringe Kopplung dereinzelnen Komponenten 
bereitstellt. Durch das Arbeiten mit weiteren Komponenten, wie dem Keypad und LCD-Display, 
lässt sich der objektorientierte Ansatz hervorragend verwenden, da die einzelnen Bibliotheken 
der Komponenten ebenfalls von deren Verfassern objektorientiert erstellt worden sind.

## Lösung und Alternativen
Während des Planungsprozesses wurde sich für die Umsetzung über ein klassisches
Keypad mit den Eingabemöglichkeiten der Zahlen von null bis einschließlich neun und
den Zeichen A, B, C, D, *, # entschieden. Dadurch können bisweilen zwischen 16 Zeichen
aus dem Eingabealphabet differenziert werden, um bei einem festgelegten vierstelligen
Pin 65536 Pinmöglichkeiten gewährleisten zu können. Diese Lösung wurde in der Phase
der Prototypenentwicklung softwaretechnisch implementiert und hardwaretechnisch simuliert.
Der Quellcode der einzelnen Klassen ist neben dem UML-Klassendiagramm dem
Anhang des Berichtes beigefügt. Das Authentifizierungssystem wird neben dem Keypad
auf der funktionalen Seite durch ein kleines LCD-Display ergänzt, welches im Normalzustand
ein Auswahlmenü mit vier Optionen anzeigt. Diese Funktionen sind einerseits die
Funktionalität des Hinzufügens und Löschens von Pincodes, andererseits die Funktionalität
des Logins sowie abschließend eine Auswahlmöglichkeit, welche das LCD-Display
abschalten und das System in einen gesicherten Zustand überführen soll. Der implementierte
Prototyp zeichnet sich durch Umsetzung des objektorientierten Ansatzes aus. Die
Implementationsklassen beider Komponenten, sowohl des Keypads als auch des LCD-Displays
erben von der aus der Arduino Standardbibliothek bereitgestellten jeweiligen
Klasse und spezifizieren Funktionen und weitere Attribute. Die Klasse CSafe, dessen
Definition dem Anhang beigefügt ist, instanziiert von beiden Komponenteklassen jeweils
eine Instanz als privaten Member. Dadurch kann diese Klasse als eine Art Verwaltungsklasse
das ganze System kontrollieren.

Eine weitere Umsetzungsmöglichkeit wäre anstatt der Authentifizierung per Keypad eine Authentifizierungsmöglichkeit per RFID-Schlüsselkarte gewesen. Diese Möglichkeit
wurde jedoch für die Implementation in einem Prototypen vernachlässigt, da die entsprechende
Komponente nicht im Simulationstool verfügbar ist und eine spätere Umsetzung
als Pincode-Schloss oftmals preiswerter als eine RFID-Schlüsselanlage ist. Ein großer
Unterschied in der Schlüsselsicherheit kann nicht festgestellt werden, da die Sicherheit
eines Zahlenschlosses von der Länge der Eingabe und die Anzahl der möglichen Zeichen im
Eingabealphabet abhängt.

Eine rein manuelle Implementationsmöglichkeit wäre die Umsetzung per Schlüsselsystem gewesen.
Hierbei können jedoch zu den oben genannten Umsetzungsmöglichkeiten
weitere Nachteile und Risiken aufgezeigt werden. Insgesamt ist ein normales Schlüsselsystem
kostspielig in der Anschaffung, aufwendig in dem Einbau, der Instandhaltung und
zudem eine äußerst unsichere Variante der Authentifizierung, da bei einem Schlüsselverlust
das gesamte Schlüsselsystem inklusive der einzelnen Schlösser ausgetauscht werden müssen.

Die hier implementierte Lösung kann jedoch nicht als vollständiges Produkt angesehen
werden, da die einzelnen Komponenten nur bedingt für deren Einsatz für diesen Zweck
geeignet sind. Ein elementarer Faktor ist die Frage der Datenhaltung der einzelnen Pincodes.
Hier sollte im späteren Entwicklungsverlauf über eine Lösung basierend auf einer
relationalen Datenbank nachgedacht werden. Des Weiteren sollten die einzelnen Pincodes 
nicht im Klartext gespeichert werden, um die genannte Sicherheit des Pincodeverfahrens 
nochmals zu erhöhen. Der Einsatz einer Hashing-Technik wäre hier sinnvoll.

## Qualitätsmanagement
Während der Projektplanung wurden ebenfalls verschiedene Aspekte des Qualitätsmanagements
geklärt. Hierbei wurde für die Entwicklung der Software das Konfigurationsmanagementsystem 
Git eingeführt, damit die Entwicklungsobjekte durchgehend als einheitlicher Stand 
für jedes Projektmitglied verfügbar waren. Neben dieser Entscheidung
hat sich das Projektteam gegen automatisierte Testverfahren, wie Modultests sowie 
Integrationstests entschieden, da diese bei der Entwicklung des Prototypens ein hoher
Zeitaufwand zur Folge und zudem keine zweckdienliche Funktion gehabt hätten. Dennoch
wurden die einzelnen Funktionen des Prototypensystems immer mindestens im
Vier-Augen-Prinzip abgenommen, wodurch ein möglichst effizientes Testmanagement
durchgeführt werden konnte.

Gegen Qualitätsanforderungen wie beispielsweise die Schnelligkeit des Systems wurde
zurzeit nicht getestet, da es sich bei dem momentan entwickelten Produkt um einen
Prototyp handelt, bei welchem Qualitätsanforderungen vernachlässigt werden können. Bei
der Entwicklung des Produktivsystems sollte jedoch darauf geachtet werden, dass diese
Anforderungen ebenfalls durch Testszenarien, wie durch Abnahmetests mit den Endnutzern
berücksichtigt werden. Die funktionalen Anforderungen in Form der User Stories
und jegliche auftretende Randbedingungen können durch die genannten Testverfahren
ganzheitlich abgedeckt werden.

Insgesamt soll das Gesamtsystem ähnlich zu dem Prototyp möglichst modular aufgebaut
werden, damit ein einfaches Testen ermöglicht wird und eine Vielzahl an
Integrationstestszenarien angewandt werden können. Des Weiteren wird sowohl der
Prototypen als auch das Endprodukt vor der Weiterführung der Projektphasen durch
den Auftraggeber abgenommen. Diese Abnahme soll ein zusätzliches Quality-Gate
auf dem Weg zu dem Rollout des Produktes sein.

Aus den genannten Testverfahren und deren Zweck ergeben sich ebenfalls die
Qualitätsmanagementziele, welche das Projektteam anfänglich definiert hat.
Die Testverfahren zielen darauf ab funktionale Anforderungen,
Qualitätsanforderungen und Randbedingungen zu testen. Sobald Tests fehlerhaft
sind, muss das entsprechende Entwicklungsobjekt überprüft und korrigiert werden.
Tests werden durch das Projektteam als erfolgreich definiert, sofern Tests keine
Verletzung von Anforderungen entdecken oder nur eine geringe Varianz zu den
Anforderungen aufweisen. Weitere Ziele des Qualitätsmanagements im Hinblick auf
den Einsatz des Produktivsystems bei dem Kunden sind eine Verbesserung der 
Systemakzeptanz bei den Endnutzern, Erhöhung der Produktivität beim Systemeinsatz 
und zudem die Approximation der Systemeigenschaften an die Kundenwünsche.

## Wirtschaftlichkeit
Ein wichtiger Aspekt dieses Projektes ist die Wirtschaftlichkeit für den Auftraggeber, dabei
erwartet der Anforderer sowohl ein qualitativ hochwertiges, aber auch kosteneffizientes
System. Der essentielle Qualitätsaspekt bei dem Endprodukt stellt die Sicherheit
dar, dieser wird jedoch durch weitere zweitrangige Faktoren, wie die Benutzerfreundlichkeit
oder die Wartbarkeit ergänzt. Die Sicherheit des Produktes kann einerseits anhand
der Authentifizierungsmöglichkeit gemessen werden, andererseits ist dieses Kriterium
ebenfalls an der Datenhaltung messbar.

Der weitere Nutzen liegt darin, dass entwickelte System nach einer erfolgreichen Testphase
für die Authentifizierung am Tresor des Kreditinstitutes zu übernehmen, aber womöglich
auch in abgewandelter Form als Zugangskontrollmechanismus für andere Bürobereiche
des Kreditinstitutes zu verwenden.

Auf der Kostenseite können Systeme in den verschiedensten Preisklassen entwickelt
werden, sodass insgesamt viele Kostenmöglichkeiten dem Kunden zur Auswahl stehen.
Jedoch ist es wichtig zu verstehen, dass eine Erhöhung der qualitativen Faktoren, hier
insbesondere der Sicherheitsaspekt, eine automatische Erhöhung der Kosten, aufgrund
anderer Komponenten, zur Folge hat. Demnach liegt es in der Pflicht des Kunden, diese
Qualitätsfaktoren im Voraus angemessen zu berücksichtigen, da eine Änderung von
Komponenten einen ungewollten, aber auch vermeidbaren Kostenaufwand als Auswirkung
mit sich ziehen.